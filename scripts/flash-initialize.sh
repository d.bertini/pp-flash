#!/bin/bash 

myhost=`hostname`

# Submit directory 
FLASH_DIR=$SLURM_SUBMIT_DIR

# Local scratch directory on the node
FLASH_TMP="/tmp/$USER/"

#echo " node: " $SLURM_NODEID " localid: " $SLURM_LOCALID 

# Copying .cn4 files to /tmp
if  [ "$SLURM_LOCALID" -eq "0" ]; then
    mkdir -p $FLASH_TMP
    cp $FLASH_DIR/*.cn4 $FLASH_TMP 
    echo `hostname`
    ls $FLASH_TMP
    echo " Flash .cn4 input files copied on " $myhost " node: " $SLURM_NODEID " local_id: " $SLURM_LOCALID 
fi

