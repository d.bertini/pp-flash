# pp-flash
This repository aims to help the usage of [FLASH](https://flash.rochester.edu/site/flashcode.html) within a stand-alone
container on [virgo](https://hpc.gsi.de/virgo/).

## Preparing singularity container
[FLASH](https://flash.rochester.edu/site/flashcode.html) needs a fortran compiler, [openMPI](https://www.open-mpi.org/),  [HDF5](https://github.com/HDFGroup/hdf5) and [Hypre](https://github.com/hypre-space/hypre)
A singularity  definition file (`/defs`) can be used to create a singularity image file containing the necessary software stack ( latest version ).
One can create the image virgo3 bare-metal submit node (`virgo3.hpc.gsi.de`).
To create the image file :

```
apptainer build rlx8_ompi_ucx_flash.sif rlx8_ompi_ucx_flash.def

```
## Compiling
To compile FLASH within the container environment, you will need to use the Makefile.h from this repository ( `/src` ) compilation is done as usual
defining a simulation model i.e

```
> export CONT=<path_to_image_location>/rlx8_ompi_ucx_flash.sif
> singularity exec $CONT bash -l
RLX system profile loaded ...
[pp_dask]~/flash $ ./setup -auto LaserSlab -2d +cylindrical -nxb=16 -nyb=16 +serialio species=cham,targ +mtmmmt +laser +uhd3t +mgd mgd_meshgroups=6 -parfile=example.par
[pp_dask]~/flash $ cd object
[pp_dask]~/flash $ make

```

## Submitting

When submitting FLASH job it is important to control how many cores/node will be used. To do that use the following template:

```
sbatch --nodes 20 --ntasks-per-node 60  --no-requeue --job-name flash_t --mem-per-cpu 2000 --mail-type ALL --mail-user username@gsi.de --time 0-08:00:00 --partition=high_mem -D . -o %j.out.log -e %j.err.log   ./run-file.sh

```

It is important not to use too much processes per node `--ntasks-per-node`  should not exceed typically 80 processes/node for the `high_mem` and 50/60 processes/node for the `long` partition


# Lustre file system corruption
If too many `flash` processes per node try to read the same input file on /lustre, the job may deadlock and the used working will not be acessible to the user anymore. 
To avoid this, once can before running the flash job to copy the files on `/tmp/$USER` and adatp his/her `flash.par` input to read from `/tmp` and not from `/lustre` anymore.
Example submit script are available on the  `/scripts` directory and an example of adapted `flash.par` is available on `/src/`
