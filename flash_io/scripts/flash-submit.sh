#!/bin/bash

#SBATCH --job-name=flash_t
#SBATCH --reservation=dbertini2
#SBATCH --time=00-08:00:00
#SBATCH --output=%j.out.log
#SBATCH --error=%j.err.log
#SBATCH --partition=long
#SBATCH --nodes=6
#SBATCH --ntasks-per-node=128
#SBATCH --mem-per-cpu=1960
# #SBATCH --mem=16gb #  N_t (threads) * requiring Mem_t (1000 MBytes)
#SBATCH --mail-type=ALL
#SBATCH --mail-user=${USER}@gsi.de
#SBATCH --no-requeue
#SBATCH --cpus-per-task=1
#SBATCH --export=ALL,EXE=./flash4


export OMP_NUM_THREADS=${SLURM_CPUS_PER_TASK}
export NUM_CORES=${SLURM_NTASKS}*${SLURM_CPUS_PER_TASK}


echo "Job configuration: "
echo "${EXE} running on ${NUM_CORES} cores with ${SLURM_NTASKS} MPI-tasks and ${OMP_NUM_THREADS} threads"
echo " "


# UCX settings
#export UCX_TLS=ib,dc,sm
#export UCX_LOG_LEVEL=debug
#export UCX_IB_RCACHE_MAX_REGIONS=1000
#export UCX_RC_MLX5_TX_NUM_GET_BYTES=256k
#export UCX_RC_MLX5_MAX_GET_ZCOPY=32k


# Apptainer settings
export CONT=/lustre/rz/dbertini2/containers_n/flash/rlx8_ompi_ucx_flash.sif
export APPTAINER_BINDPATH=/lustre/rz/dbertini_testing,/cvmfs
export APPTAINER_SHARENS=true
export APPTAINER_CONFIGDIR=/tmp/$USER

# OMPI settings
export OMPI_MCA_io=romio341

# define scripts to run
init_script=flash-initialize.sh
run_script=flash-run.sh
finalize_script=flash-finalize.sh

# run tasks-scripts
#srun --export=ALL  ./$init_script 
srun --export=ALL  --cpu-bind=cores apptainer  exec  ${CONT} ./$run_script 
#srun --export=ALL  ./$finalize_script 
