#!/bin/bash 

myhost=`hostname`

# Submit directory 
FLASH_DIR=$SLURM_SUBMIT_DIR

# Local scratch directory on the node
FLASH_TMP="/tmp/$USER/"

# Copying .cn4 files to /tmp
if  [ "$SLURM_LOCALID" -eq "0" ]; then
    rm -rf  $FLASH_TMP/*.cn4  
    echo " Flash .cn4 input files removed on " $myhost  " at location: " $FLASH_TMP " node: " $SLURM_NODEID " local_id: " $SLURM_LOCALID 
fi

