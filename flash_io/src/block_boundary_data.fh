!
! This file defines a data structure to be used for quantities
! which may need to be defined at grid block interfaces, eg fluxes,
! pressures.
!


! storage used for fluxes at block boundaries. This is used when conservation
! constraints need to be imposed.

c updated 2-15-00 -- allocate storage for the internal energy flux
      integer nfluxvar
      parameter(nfluxvar=nuc2+7) !<<< USER EDIT

      integer nfluxes
      parameter(nfluxes=max(1,nfluxvar))

      integer maxblocksfl
      parameter(maxblocksfl= 1+(maxblocks-1)*min(1,nfluxvar) )



c..in 1d the flux_y, flux_z, tflux_y, and tflux_z arrays are not used,
c..but do need to be declared. thus, in 1d the parameter maxblocksfl
c..has been replaced with a 1. this significantly reduces the
c..memory footprint for 1d simulations.

c..in 2d the flux_z and tflux_z arrays are not used,
c..but do need to be declared. thus, in 2d the parameter maxblocksfl
c..has been replaced with a 1. this significantly reduces the
c..memory footprint for 2d simulations.

#if N_DIM == 1
      common/fluxes/ 
     . flux_x(nfluxes,1:2,jl_bnd:ju_bnd,kl_bnd:ku_bnd,maxblocksfl),
     . flux_y(nfluxes,il_bnd:iu_bnd,1:2,kl_bnd:ku_bnd,1),
     . flux_z(nfluxes,il_bnd:iu_bnd,jl_bnd:ju_bnd,1:2,1)
      real flux_x,flux_y,flux_z

      common/tfluxes/ 
     .  tflux_x(nfluxes,1:2,jl_bnd:ju_bnd,kl_bnd:ku_bnd,maxblocksfl)
     . ,tflux_y(nfluxes,il_bnd:iu_bnd,1:2,kl_bnd:ku_bnd,1)
     . ,tflux_z(nfluxes,il_bnd:iu_bnd,jl_bnd:ju_bnd,1:2,1)
      real tflux_x,tflux_y,tflux_z
#endif


#if N_DIM == 2
      common/fluxes/ 
     . flux_x(nfluxes,1:2,jl_bnd:ju_bnd,kl_bnd:ku_bnd,maxblocksfl),
     . flux_y(nfluxes,il_bnd:iu_bnd,1:2,kl_bnd:ku_bnd,maxblocksfl),
     . flux_z(nfluxes,il_bnd:iu_bnd,jl_bnd:ju_bnd,1:2,1)
      real flux_x,flux_y,flux_z

      common/tfluxes/ 
     .  tflux_x(nfluxes,1:2,jl_bnd:ju_bnd,kl_bnd:ku_bnd,maxblocksfl)
     . ,tflux_y(nfluxes,il_bnd:iu_bnd,1:2,kl_bnd:ku_bnd,maxblocksfl)
     . ,tflux_z(nfluxes,il_bnd:iu_bnd,jl_bnd:ju_bnd,1:2,1)
      real tflux_x,tflux_y,tflux_z
#endif


#if N_DIM == 3
      common/fluxes/ 
     . flux_x(nfluxes,1:2,jl_bnd:ju_bnd,kl_bnd:ku_bnd,maxblocksfl),
     . flux_y(nfluxes,il_bnd:iu_bnd,1:2,kl_bnd:ku_bnd,maxblocksfl),
     . flux_z(nfluxes,il_bnd:iu_bnd,jl_bnd:ju_bnd,1:2,maxblocksfl)
      real flux_x,flux_y,flux_z

      common/tfluxes/ 
     .  tflux_x(nfluxes,1:2,jl_bnd:ju_bnd,kl_bnd:ku_bnd,maxblocksfl)
     . ,tflux_y(nfluxes,il_bnd:iu_bnd,1:2,kl_bnd:ku_bnd,maxblocksfl)
     . ,tflux_z(nfluxes,il_bnd:iu_bnd,jl_bnd:ju_bnd,1:2,maxblocksfl)
      real tflux_x,tflux_y,tflux_z
#endif



! storage used for cell edges at block boundaries. 
! This is used when quantities located at cell edge centers need to
! be used consistently at the boundaries between blocks at different
! refinement levels.

      integer nedgevar
      parameter(nedgevar=1)                                     !<<< USER EDIT

      integer nedges
      parameter(nedges=max(1,nedgevar))

      integer maxblockse
      parameter(maxblockse= 1+(maxblocks-1)*min(1,nedgevar) )


c..flash does not presently use these edge storage variables
c..but it might when magnetic fields are included. until then,
c..the maxblockse size declaration has been replaced with 1 in order
c..to reduce the memory footprint.

c      common/edges/
c     .  bedge_facex_y(nedges,1:2,jl_bnd:ju_bnd+1,
c     .    kl_bnd:ku_bnd+1,maxblockse),
c     .  bedge_facex_z(nedges,1:2,jl_bnd:ju_bnd+1,
c     .    kl_bnd:ku_bnd+1,maxblockse),
c     .  bedge_facey_x(nedges,il_bnd:iu_bnd+1,1:2,
c     .    kl_bnd:ku_bnd+1,maxblockse),
c     .  bedge_facey_z(nedges,il_bnd:iu_bnd+1,1:2,
c     .    kl_bnd:ku_bnd+1,maxblockse),
c     .  bedge_facez_x(nedges,il_bnd:iu_bnd+1,jl_bnd:ju_bnd+1,
c     .    1:2,maxblockse),
c     .  bedge_facez_y(nedges,il_bnd:iu_bnd+1,jl_bnd:ju_bnd+1,
c     .    1:2,maxblockse),


      common/edges/
     .  bedge_facex_y(nedges,1:2,jl_bnd:ju_bnd+1,
     .    kl_bnd:ku_bnd+1,1),
     .  bedge_facex_z(nedges,1:2,jl_bnd:ju_bnd+1,
     .    kl_bnd:ku_bnd+1,1),
     .  bedge_facey_x(nedges,il_bnd:iu_bnd+1,1:2,
     .    kl_bnd:ku_bnd+1,1),
     .  bedge_facey_z(nedges,il_bnd:iu_bnd+1,1:2,
     .    kl_bnd:ku_bnd+1,1),
     .  bedge_facez_x(nedges,il_bnd:iu_bnd+1,jl_bnd:ju_bnd+1,
     .    1:2,1),
     .  bedge_facez_y(nedges,il_bnd:iu_bnd+1,jl_bnd:ju_bnd+1,
     .    1:2,1),
     .  recvarx1e(nedges,1:2,jl_bnd:ju_bnd+1,kl_bnd:ku_bnd+1),
     .  recvary1e(nedges,il_bnd:iu_bnd+1,1:2,kl_bnd:ku_bnd+1),
     .  recvarz1e(nedges,il_bnd:iu_bnd+1,jl_bnd:ju_bnd+1,1:2),
     .  recvarx2e(nedges,1:2,jl_bnd:ju_bnd+1,kl_bnd:ku_bnd+1),
     .  recvary2e(nedges,il_bnd:iu_bnd+1,1:2,kl_bnd:ku_bnd+1),
     .  recvarz2e(nedges,il_bnd:iu_bnd+1,jl_bnd:ju_bnd+1,1:2)
      real bedge_facex_y,bedge_facex_z,bedge_facey_x
      real bedge_facey_z,bedge_facez_x,bedge_facez_y
      real recvarx1e,recvary1e,recvarz1e
      real recvarx2e,recvary2e,recvarz2e


c..flash does not presently use these edge storage variables
c..but it might when mhd is added in.
c..the maxblockse size declaration has been reduced to 1 in order
c..to help reduce the memory footprint.
c..28nov99 fxt


c      common/tedges/
c     .  tbedge_facex_y(nedges,1:2,jl_bnd:ju_bnd+1,
c     .    kl_bnd:ku_bnd+1,maxblockse),
c     .  tbedge_facex_z(nedges,1:2,jl_bnd:ju_bnd+1,
c     .    kl_bnd:ku_bnd+1,maxblockse),
c     .  tbedge_facey_x(nedges,il_bnd:iu_bnd+1,1:2,
c     .    kl_bnd:ku_bnd+1,maxblockse),
c     .  tbedge_facey_z(nedges,il_bnd:iu_bnd+1,1:2,
c     .    kl_bnd:ku_bnd+1,maxblockse),
c     .  tbedge_facez_x(nedges,il_bnd:iu_bnd+1,jl_bnd:ju_bnd+1,
c     .    1:2,maxblockse),
c     .  tbedge_facez_y(nedges,il_bnd:iu_bnd+1,jl_bnd:ju_bnd+1,
c     .    1:2,maxblockse)
      
      common/tedges/
     .  tbedge_facex_y(nedges,1:2,jl_bnd:ju_bnd+1,
     .    kl_bnd:ku_bnd+1,1),
     .  tbedge_facex_z(nedges,1:2,jl_bnd:ju_bnd+1,
     .    kl_bnd:ku_bnd+1,1),
     .  tbedge_facey_x(nedges,il_bnd:iu_bnd+1,1:2,
     .    kl_bnd:ku_bnd+1,1),
     .  tbedge_facey_z(nedges,il_bnd:iu_bnd+1,1:2,
     .    kl_bnd:ku_bnd+1,1),
     .  tbedge_facez_x(nedges,il_bnd:iu_bnd+1,jl_bnd:ju_bnd+1,
     .    1:2,1),
     .  tbedge_facez_y(nedges,il_bnd:iu_bnd+1,jl_bnd:ju_bnd+1,
     .    1:2,1)
      real tbedge_facex_y,tbedge_facex_z,tbedge_facey_x
      real tbedge_facey_z,tbedge_facez_x,tbedge_facez_y



! workspace arrays used for inter-block communications
        integer nbndmax
        parameter(nbndmax=max(nbndvar,nfluxes))
        common/blockbnd/
     .     recvarx1(nbndmax,1:2,jl_bnd:ju_bnd,kl_bnd:ku_bnd)
     .    ,recvary1(nbndmax,il_bnd:iu_bnd,1:2,kl_bnd:ku_bnd)
     .    ,recvarz1(nbndmax,il_bnd:iu_bnd,jl_bnd:ju_bnd,1:2)
     .    ,bndtempx1(nfluxes,1:2,jl_bnd:ju_bnd,kl_bnd:ku_bnd)
     .    ,bndtempy1(nfluxes,il_bnd:iu_bnd,1:2,kl_bnd:ku_bnd)
     .    ,bndtempz1(nfluxes,il_bnd:iu_bnd,jl_bnd:ju_bnd,1:2)
        real    recvarx1,recvary1,recvarz1
        real    bndtempx1,bndtempy1,bndtempz1




! parameters used in communication calls
      integer len_block_bndx,len_block_bndy,len_block_bndz
      parameter(len_block_bndx=2*(nyb+2*nguard*k2d)*
     .                                 (nzb+2*nguard*k3d))
      parameter(len_block_bndy=2*(nxb+2*nguard*k2d)*
     .                                 (nzb+2*nguard*k3d))
      parameter(len_block_bndz=2*(nxb+2*nguard)*(nyb+2*nguard))

      integer len_block_ex,len_block_ey,len_block_ez
      parameter(len_block_ex=2*(nyb+k2d+2*nguard*k2d)*
     .                                 (nzb+k3d+2*nguard*k3d))
      parameter(len_block_ey=2*(nxb+1+2*nguard)*
     .                                 (nzb+k3d+2*nguard*k3d))
      parameter(len_block_ez=2*(nxb+1+2*nguard)*
     .                                 (nyb+k2d+2*nguard))
