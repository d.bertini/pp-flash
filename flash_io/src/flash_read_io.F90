    subroutine read_table_static( tableName, fUnit)
      use mpi 
      use Eos_data,         ONLY : eos_smallT
      use eos_tabData,      ONLY :  eos_useLogTables,          &
                                    EOS_TAB_NCOMP,         &
                                    EOS_TAB_NALLTAB,         &
                                    EOS_TAB_FOR_ION,         &
                                    EOS_TAB_FOR_ELE,         &
                                    EOS_TAB_FOR_MAT,         &
                                    EOS_TABVT_ZF,         &
                                    EOS_TABVT_EN,         &
                                    EOS_TABVT_PR,         &
                                    EOS_TABVT_HC,         &
                                    EOS_TABVT_ENTR,       &
                                    eosT_tableGroupDescT, &
                                    eosT_oneVarTablePT
      
#include "Eos.h"

      logical  :: wanted(EOS_TAB_NCOMP,EOS_TAB_NALLTAB)
      character (len=80), intent (in) :: tableName
      integer errcode, ierr
      character (len=80) :: dummyLine
      
      logical, parameter :: needZFTable=.true.
      logical, parameter :: needENTables=.true.
      logical, parameter :: needPRTables=.true.
      logical, parameter :: needHCTables=.true.
      logical, parameter :: needEntrTables=.true.
      logical :: fileExists, doread
      
      integer :: ntemp, ndens
      real, allocatable :: temperatures(:)
      real, allocatable :: densities(:)
      integer :: fileUnit
      integer :: notneededData
      integer :: step
      integer :: i,j,n,t,d,g
      integer :: ngroupsEnergy
      integer :: nstepsDensity
      integer :: nstepsTemperature
      integer :: ut_getFreeFileUnit
      
      real    :: dummyData,tt
      real    :: log10DensityStep
      real    :: log10DensityFirst
      real    :: log10TemperatureStep
      real    :: log10TemperatureFirst
      real    :: maxTempStepFactor,maxDensStepFactor
      real    :: minTempStepFactor,minDensStepFactor

      type(eosT_tableGroupDescT), allocatable :: td(:)
      type(eosT_oneVarTablePT),pointer,dimension(:) :: tbZF,tbEN,tbPR,tbHC,tbEntr

      ! IO paramter
      integer :: io, stat
      character(len=512) :: msg
      
      ! Conversion from eV to K:
      real, parameter :: K = 11604.5221
      real, parameter :: joule2erg = 1.0e7
      integer, intent(in) :: fUnit
      integer MyPE, NumPEs, MasterPE

      call MPI_Comm_Rank (MPI_Comm_World, MyPE, ierr)
      call MPI_Comm_Size (MPI_Comm_World, NumPEs, ierr)
      
      print*, 'rank:', MyPE, ' inquiring file: ', tableName       
      inquire (file = tableName , exist = fileExists)
      
      if (.not.fileExists) then
         call MPI_Abort(MPI_COMM_WORLD, errcode, ierr)
         stop 
      end if
       
      ! Open the file in read mode
      print*, 'rank:', MyPE, ' opening file: ', tableName 
      open (unit=fUnit , file = tableName, iostat=stat, iomsg=msg)
      if (stat /= 0) then
         print *, 'error opening file msg:', trim(msg)
         call MPI_Abort(MPI_COMM_WORLD, errcode, ierr)
         stop          
      end if
      print*, 'rank:', MyPE, ' opening file done... '      

      print*, 'rank:', MyPE, ' reading file: ', tableName       
      read (fUnit,'(2I10)') nstepsTemperature , nstepsDensity
      print*, ' read step_temperature: ', nstepsTemperature, ' density: ', nstepsDensity

      ntemp = nstepsTemperature
      ndens = nstepsDensity
      read (fUnit,'(A80)')  dummyLine
      read (fUnit,'(A80)')  dummyLine
      read (fUnit,'(I12)') ngroupsEnergy

      if (nstepsTemperature <= 0) then
         call MPI_Abort(MPI_COMM_WORLD, errcode, ierr)
      end if
      
      if (nstepsDensity <= 0) then
         call MPI_Abort(MPI_COMM_WORLD, errcode, ierr)
      end if
      
      if (ngroupsEnergy < 0) then
        print*, 'found ngroupsEnergy < 0'
      end if

      print*, ' ngroupsEnergy: ', ngroupsEnergy
      
      allocate(temperatures(ntemp))
      allocate(densities(ndens))
      
      read (fUnit,'(4E12.6)') (temperatures(t),t=1,ntemp)
      ! Convert the temperatures into K from eV:
      temperatures(:) = temperatures(:) * K

      do t = 1,ntemp-1
         print*, 'it: ', t, ' temperature: ', temperatures(t) 
      end do


      log10TemperatureFirst = log10(Temperatures(1))
      minTempStepFactor = HUGE(minTempStepFactor)
      maxTempStepFactor = 1

      do t = 1,ntemp-1
         if(temperatures(t) < eos_smallT) then 
            if(temperatures(t+1) > eos_smallT) &
                 minTempStepFactor = min(minTempStepFactor,temperatures(t+1)/eos_smallT)
            maxTempStepFactor = max(maxTempStepFactor,temperatures(t+1)/eos_smallT)
         else
            minTempStepFactor = min(minTempStepFactor,temperatures(t+1)/temperatures(t))
            maxTempStepFactor = max(maxTempStepFactor,temperatures(t+1)/temperatures(t))
         end if
      end do
      
      read (fUnit,'(4E12.6)') (densities(d),d=1,ndens)

      log10DensityFirst = log10(densities(1))
      minDensStepFactor = HUGE(minDensStepFactor)
      maxDensStepFactor = 1

      do d = 1,ndens-1
         minDensStepFactor = min(maxDensStepFactor,densities(d+1)/densities(d))
         maxDensStepFactor = max(maxDensStepFactor,densities(d+1)/densities(d))
      end do

      ! ZFTAble Dummy
      if (needZFTable) then
         read (fUnit,'(4E12.6)') ((dummyData, i = 1,nstepsTemperature), j = 1,nstepsDensity )
         !   ...Skip not needed data from the IONMIX4 file.
         notneededData =   nstepsDensity * nstepsTemperature
         read (fUnit,'(4E12.6)') (dummyData, i = 1,notneededData)
      end if


      ! PR tables dummy
      if (needPRTables) then
         do n=EOS_TAB_FOR_ION,EOS_TAB_FOR_ION+1
            doread = (n .LE. EOS_TAB_NCOMP)
            if (doread) then
               read (fUnit,'(4E12.6)') ((dummyData, t = 1,nstepsTemperature), d = 1,nstepsDensity )
            end if
            !   ...Skip not needed data from the IONMIX4 file.
            notneededData =   nstepsDensity * nstepsTemperature
            read (fUnit,'(4E12.6)') (dummyData, i = 1,notneededData)
         end do
      else
         !   ...Skip not needed data from the IONMIX4 file.
         notneededData =   nstepsDensity * nstepsTemperature
         read (fUnit,'(4E12.6)') (dummyData, i = 1,notneededData)
         read (fUnit,'(4E12.6)') (dummyData, i = 1,notneededData)
      end if

      ! ...Skip dPion/dTion table
      notneededData =   nstepsDensity * nstepsTemperature
      read (fUnit,'(4E12.6)') (dummyData, i = 1,notneededData)
      
      ! ...Skip dPele/dTele table
      notneededData =   nstepsDensity * nstepsTemperature
      read (fUnit,'(4E12.6)') (dummyData, i = 1,notneededData)

      
      deallocate(temperatures)
      deallocate(densities)

      print*, 'rank:', MyPE, ' closing file: ', tableName                            
      close (fUnit)
      print*, 'rank:', MyPE, ' closing file done '
      
    end subroutine read_table_static


    subroutine read_table_dyn( tableName )
      use mpi 
      use Eos_data,         ONLY : eos_smallT
      use eos_tabData,      ONLY :  eos_useLogTables,          &
                                    EOS_TAB_NCOMP,         &
                                    EOS_TAB_NALLTAB,         &
                                    EOS_TAB_FOR_ION,         &
                                    EOS_TAB_FOR_ELE,         &
                                    EOS_TAB_FOR_MAT,         &
                                    EOS_TABVT_ZF,         &
                                    EOS_TABVT_EN,         &
                                    EOS_TABVT_PR,         &
                                    EOS_TABVT_HC,         &
                                    EOS_TABVT_ENTR,       &
                                    eosT_tableGroupDescT, &
                                    eosT_oneVarTablePT
      
#include "Eos.h"

      logical  :: wanted(EOS_TAB_NCOMP,EOS_TAB_NALLTAB)
      character (len=80), intent (in) :: tableName
      integer errcode, ierr
      character (len=80) :: dummyLine
      
      logical, parameter :: needZFTable=.true.
      logical, parameter :: needENTables=.true.
      logical, parameter :: needPRTables=.true.
      logical, parameter :: needHCTables=.true.
      logical, parameter :: needEntrTables=.true.
      logical :: fileExists, doread
      
      integer :: ntemp, ndens
      real, allocatable :: temperatures(:)
      real, allocatable :: densities(:)
      integer :: fileUnit
      integer :: notneededData
      integer :: step
      integer :: i,j,n,t,d,g
      integer :: ngroupsEnergy
      integer :: nstepsDensity
      integer :: nstepsTemperature
      integer :: ut_getFreeFileUnit
      
      real    :: dummyData,tt
      real    :: log10DensityStep
      real    :: log10DensityFirst
      real    :: log10TemperatureStep
      real    :: log10TemperatureFirst
      real    :: maxTempStepFactor,maxDensStepFactor
      real    :: minTempStepFactor,minDensStepFactor

      type(eosT_tableGroupDescT), allocatable :: td(:)
      type(eosT_oneVarTablePT),pointer,dimension(:) :: tbZF,tbEN,tbPR,tbHC,tbEntr

      ! IO paramter
      integer :: io, stat
      character(len=512) :: msg
      
      ! Conversion from eV to K:
      real, parameter :: K = 11604.5221
      real, parameter :: joule2erg = 1.0e7
      integer MyPE, NumPEs, MasterPE

      call MPI_Comm_Rank (MPI_Comm_World, MyPE, ierr)
      call MPI_Comm_Size (MPI_Comm_World, NumPEs, ierr)
      
      print*, 'rank:', MyPE, ' inquiring file: ', tableName       
      inquire (file = tableName , exist = fileExists)
      
      if (.not.fileExists) then
         call MPI_Abort(MPI_COMM_WORLD, errcode, ierr)
         stop 
      end if
       
      ! Open the file in read mode
      print*, 'rank:', MyPE, ' opening file: ', tableName 
      open (newunit=io , file = tableName, action='read', iostat=stat, iomsg=msg)
      if (stat /= 0) then
         print *, 'error opening file msg:', trim(msg)
         call MPI_Abort(MPI_COMM_WORLD, errcode, ierr)
         stop          
      end if
      print*, 'rank:', MyPE, ' opening file done... '      

      print*, 'rank:', MyPE, ' reading file: ', tableName       
      read (io,'(2I10)') nstepsTemperature , nstepsDensity
      print*, ' read step_temperature: ', nstepsTemperature, ' density: ', nstepsDensity

      ntemp = nstepsTemperature
      ndens = nstepsDensity
      read (io,'(A80)')  dummyLine
      read (io,'(A80)')  dummyLine
      read (io,'(I12)') ngroupsEnergy

      if (nstepsTemperature <= 0) then
         call MPI_Abort(MPI_COMM_WORLD, errcode, ierr)
      end if
      
      if (nstepsDensity <= 0) then
         call MPI_Abort(MPI_COMM_WORLD, errcode, ierr)
      end if
      
      if (ngroupsEnergy < 0) then
        print*, 'found ngroupsEnergy < 0'
      end if

      print*, ' ngroupsEnergy: ', ngroupsEnergy
      
      allocate(temperatures(ntemp))
      allocate(densities(ndens))
      
      read (io,'(4E12.6)') (temperatures(t),t=1,ntemp)
      ! Convert the temperatures into K from eV:
      temperatures(:) = temperatures(:) * K

      do t = 1,ntemp-1
         print*, 'it: ', t, ' temperature: ', temperatures(t) 
      end do


      log10TemperatureFirst = log10(Temperatures(1))
      minTempStepFactor = HUGE(minTempStepFactor)
      maxTempStepFactor = 1

      do t = 1,ntemp-1
         if(temperatures(t) < eos_smallT) then 
            if(temperatures(t+1) > eos_smallT) &
                 minTempStepFactor = min(minTempStepFactor,temperatures(t+1)/eos_smallT)
            maxTempStepFactor = max(maxTempStepFactor,temperatures(t+1)/eos_smallT)
         else
            minTempStepFactor = min(minTempStepFactor,temperatures(t+1)/temperatures(t))
            maxTempStepFactor = max(maxTempStepFactor,temperatures(t+1)/temperatures(t))
         end if
      end do
      
      read (io,'(4E12.6)') (densities(d),d=1,ndens)

      log10DensityFirst = log10(densities(1))
      minDensStepFactor = HUGE(minDensStepFactor)
      maxDensStepFactor = 1

      do d = 1,ndens-1
         minDensStepFactor = min(maxDensStepFactor,densities(d+1)/densities(d))
         maxDensStepFactor = max(maxDensStepFactor,densities(d+1)/densities(d))
      end do

      ! ZFTAble Dummy
      if (needZFTable) then
         read (io,'(4E12.6)') ((dummyData, i = 1,nstepsTemperature), j = 1,nstepsDensity )
         !   ...Skip not needed data from the IONMIX4 file.
         notneededData =   nstepsDensity * nstepsTemperature
         read (io,'(4E12.6)') (dummyData, i = 1,notneededData)
      end if


      ! PR tables dummy
      if (needPRTables) then
         do n=EOS_TAB_FOR_ION,EOS_TAB_FOR_ION+1
            doread = (n .LE. EOS_TAB_NCOMP)
            if (doread) then
               read (io,'(4E12.6)') ((dummyData, t = 1,nstepsTemperature), d = 1,nstepsDensity )
            end if
            !   ...Skip not needed data from the IONMIX4 file.
            notneededData =   nstepsDensity * nstepsTemperature
            read (io,'(4E12.6)') (dummyData, i = 1,notneededData)
         end do
      else
         !   ...Skip not needed data from the IONMIX4 file.
         notneededData =   nstepsDensity * nstepsTemperature
         read (io,'(4E12.6)') (dummyData, i = 1,notneededData)
         read (io,'(4E12.6)') (dummyData, i = 1,notneededData)
      end if

      ! ...Skip dPion/dTion table
      notneededData =   nstepsDensity * nstepsTemperature
      read (io,'(4E12.6)') (dummyData, i = 1,notneededData)
      
      ! ...Skip dPele/dTele table
      notneededData =   nstepsDensity * nstepsTemperature
      read (io,'(4E12.6)') (dummyData, i = 1,notneededData)

      
      deallocate(temperatures)
      deallocate(densities)

      print*, 'rank:', MyPE, ' closing file: ', tableName                            
      close (io)
      print*, 'rank:', MyPE, ' closing file done '
      
    end subroutine read_table_dyn


      program flash_read_io
!
! This is a sample program that setups the FLASH data structures and 
! drives the I/O routines.  It is intended for benchmarking the I/O
!performance.
!

!the main data structures are contained in common blocks, defined in the
!include files
!#include "common.fh"
      use mpi

      integer ierr
      integer i
      integer ( kind= 4 ) MyPE, NumPEs, MasterPE
      real time_io, time_begin
      character (len=80) :: path      
      character (len=80) :: cn4_1, cn4_2
      character (len=80) :: cn4_3, cn4_4      

      !initialize MPI and get the rank and size
      call MPI_INIT(ierr)      
      call MPI_Comm_Rank (MPI_Comm_World, MyPE, ierr)
      call MPI_Comm_Size (MPI_Comm_World, NumPEs, ierr)
      
      ! Lustre
      cn4_1 = "./cn4/graphite_7832_0c66eV.cn4"
      cn4_2 = "./cn4/he-imx-005.cn4"
      cn4_3 = "./cn4/hydrogen_5251_0c16eV.cn4"
      cn4_4 = "./cn4/polystyrene_7593.cn4"

      ! TMP
      !cn4_1 = "/tmp/dbertini/cn4/graphite_7832_0c66eV.cn4"
      !cn4_2 = "/tmp/dbertini/cn4/he-imx-005.cn4"
      !cn4_3 = "/tmp/dbertini/cn4/hydrogen_5251_0c16eV.cn4"
      !cn4_4 = "/tmp/dbertini/cn4/polystyrene_7593.cn4"
      
      MasterPE = 0
      
      if (MyPE .EQ. MasterPE) then
         print *, 'HDF 5 v 1.14 read test on Virgo/Lustre'
         print *, NumPEs, ' processors'
      endif

      print *, ' rank: ' , MyPE, ' reading: ' , cn4_1
      call read_table_static(cn4_1, 1)
      !call read_table_dyn(cn4_1)
      print *, ' rank: ' , MyPE, ' reading: ' , cn4_2
      call read_table_static(cn4_2, 2)
      !call read_table_dyn(cn4_2)
      print *, ' rank: ' , MyPE, ' reading: ' , cn4_3
      call read_table_static(cn4_3, 3)
      !call read_table_dyn(cn4_3)
      print *, ' rank: ' , MyPE, ' reading: ' , cn4_4
      call read_table_static(cn4_4, 4)            
      !call read_table_dyn(cn4_4)
      
      call MPI_Finalize(ierr)

    end program flash_read_io





