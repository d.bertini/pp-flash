# FLASH I/O benchmarks

This a modified/adapted version of the "old" official [FLASH I/O benchmark suite](https://www.ucolick.org/~zingale/flash_benchmark_io/).
It now works with basicaly latest compiler and libraries provided by the dedicate flash container from this repository i.e

-  GNU fortran 13.1
-  openMPI 5.0.3
-  HDF5  1.14.3

It contains now both read/write benchmarks that mimic the FLASH read procedure used during initialisation and the parallel write of dump/checkpoint
using HDF5.


# FLASH write benchmark
Compile the flash_io write benchmark using HDF5 parallel with

```
> make flash_write_io

```

# FLASH read benchmark
Compile the flash_io write benchmark using HDF5 parallel with

```
> make flash_read_io

```

# Execute FLASH benchmarks

Copy the compiled executable (read/write) in your slurm submit directory and copy the scripts from the
`scripts` sub-directory. The benchmark you can then run using:

```
> sbatch flash-submit.sh

```

