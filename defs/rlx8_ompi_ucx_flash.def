Bootstrap: docker
From: rockylinux:8.9

%labels
    APPLICATION_NAME Plasma Physics Simulation
    AUTHOR_NAME Denis Bertini
    AUTHOR_EMAIL D.Bertini@gsi.de
    YEAR 2024

%help
    Container for Plasma Physics Simulations. 
    It includes latest version of openMPI compiled with:
              - latest UCX library
              - latest Lustre 2.15 client
    in order to use efficiently on Virgo2:
            - the Infiniband interconnect Fabric.
	    - the Lustre filesystem
%files
    /lustre/rz/dbertini2/containers/flash/ci_ompi.tar /opt/ci_ompi.tar

%environment
    # Enable gcc-toolset
    source scl_source enable gcc-toolset-13
    
    export OMPI_DIR=/usr/local
    export SINGULARITY_OMPI_DIR=$OMPI_DIR
    export SINGULARITYENV_APPEND_PATH=$OMPI_DIR/bin
    export SINGULAIRTYENV_APPEND_LD_LIBRARY_PATH=$OMPI_DIR/lib
    export PATH=$OMPI_DIR/bin:$PATH
    export LD_LIBRARY_PATH=$OMPI_DIR/lib:$LD_LIBRARY_PATH
    export PATH=/usr/local/bin:$PATH
    export LD_LIBRARY_PATH=/usr/local/lib:/usr/local/lib64:$LD_LIBRARY_PATH    
    # openMPI specifics
    export OMPI_MCA_io=romio341
    
    # epoch + openpmd-api
    export PY_API=3.12
    export PYTHONPATH=/root/.local/lib/python$PY_API/site-packages/
    export PYTHONPATH=/usr/local/lib/python$PY_API/site-packages/:$PYTHONPATH
    export PYTHONPATH=/usr/local/lib64/python$PY_API/site-packages/:$PYTHONPATH    
    export PYTHONPATH=/opt/opmd/openPMD-api/build/lib64/python$PY_API/site-packages:$PYTHONPATH
    export PYTHONPATH=/opt/epoch/python$PY_API:$PYTHONPATH
    export PYTHONPATH=/opt/epoch/python$PY_API/sdf-1.0-py$PY_API-linux-x86_64.egg/:$PYTHONPATH   

  
%post

   #
   # Base upstream software
   #
    
   ## Packages needed but lustre-client
    dnf install -y epel-release  
    dnf install -y dkms    
    dnf -y update
    
   ## Base rlx8 
    dnf install -y dnf-plugins-core
    dnf config-manager --set-enabled powertools
    dnf groupinstall -y 'Development Tools'
    dnf install -y wget git bash hostname gcc gcc-gfortran gcc-c++ libatomic make file    
    dnf install -y autoconf automake libtool zlib-devel
    dnf install -y libmnl lsof numactl-libs ethtool tcl tk sqlite-devel
    dnf install -y gnuplot
    dnf install -y texlive-latex texlive-epstopdf
    dnf install -y lapack-devel blas-devel    

   ## Add-ons for python's packages
    dnf install -y openssl-devel bzip2-devel libffi-devel
    dnf install alternatives

    ## Install packages required by Slurm
    dnf install -y munge munge-devel munge-libs	
    dnf install -y mariadb-server mariadb-devel
    dnf install -y pam-devel perl readline-devel perl-Switch
    dnf install -y perl-HTML-Parser perl-libwww-perl

  ## Packages required for OpenMPI and PMIx
    dnf install -y libnl3 libnl3-devel
    dnf install -y libevent libevent-devel
    dnf install -y rdma-core-devel
    dnf install -y hwloc-devel
    dnf install -y libibverbs-devel
    
   # Re-install the python
    export PY_API=3.12
    export PY_VERSION=3.12.3
    rm -rf /opt/python
    mkdir -p /opt/python
    cd /opt/python
    
    wget -c https://www.python.org/ftp/python/$PY_VERSION/Python-$PY_VERSION.tgz
    tar -xzf Python-$PY_VERSION.tgz

    cd Python-$PY_VERSION
    ./configure --enable-optimizations --enable-loadable-sqlite-extensions
    #make -j$(nproc) altinstall
    make  altinstall	

   # rm -rf /opt/python
   #
   # Install python packages
   #
   alternatives --install /usr/bin/python upy2 /usr/local/bin/python3.12 99
   alternatives --install /usr/bin/python3 upy3 /usr/local/bin/python3.12 99   

   #
   # Install python packages
   # using virtual environment
   #

   # Initiate environment
   python -m pip --no-cache-dir install virtualenv   

   mkdir /venv
   virtualenv /venv/plasma
   source /venv/plasma/bin/activate

   echo '. /venv/plasma/bin/activate' >> $SINGULARITY_ENVIRONMENT

   ## Update python pip.
     python3 -m pip --no-cache-dir install --upgrade pip
     python3 -m pip --no-cache-dir install setuptools --upgrade

   ## Install Data Science packages
     python3 -m pip --no-cache-dir install ipython jupyterlab   
     python3 -m pip --no-cache-dir install numpy pandas h5py pyarrow scikit-learn  statsmodels matplotlib seaborn plotly yt
     python3 -m pip --no-cache-dir install recommonmark   
     python3 -m pip --no-cache-dir install networkx sphinx sphinx-rtd-theme
     python3 -m pip --no-cache-dir install datashader

   ## Install package specific to PP 
     python3 -m pip --no-cache-dir install lmfit
     python3 -m pip --no-cache-dir install "dask[complete]"
     python3 -m pip --no-cache-dir install dask-jobqueue --upgrade
     python3 -m pip --no-cache-dir install dask-labextension	

    CUSTOM_ENV=/.singularity.d/env/99-zz_custom_env.sh
    cat >$CUSTOM_ENV <<EOF
#!/bin/bash
PS1="[pp_dask]\w \$ "
EOF
    chmod 755 $CUSTOM_ENV


   ## Lustre Client
cat <<EOF >/etc/yum.repos.d/lustre-client.repo
[lustre-client]
name=Lustre Client
gpgcheck=0
baseurl=http://downloads.whamcloud.com/public/lustre/lustre-2.15.4/el8.9/client
EOF
yum install -y lustre-client-devel


   ## Prior to compilation
   ## Install and enable gcc-toolset
   dnf install -y gcc-toolset-13
   source scl_source enable gcc-toolset-13

   #
   # PMIx 
   #

   export VPMIX_VERSION=5.0.2	
   echo "Installing PMIx version: " $VPMIX_VERSION	
   mkdir -p /opt/pmix
   cd /opt/pmix
   wget -c https://github.com/openpmix/openpmix/releases/download/v$VPMIX_VERSION/pmix-$VPMIX_VERSION.tar.gz
   tar xf pmix-$VPMIX_VERSION.tar.gz
   cd pmix-$VPMIX_VERSION
   # Add headers and tests for PMIx
   ./configure --prefix=/usr/local --with-munge=/usr --with-pmix-headers --with-tests-examples && \
   make -j$(nproc)
   make -j$(nproc) install      
   rm -rf /opt/pmix

   #
   # UCX
   #

   export UCX_VERSION=1.16.0
   echo "Installing UCX version:  " $UCX_VERSION
   cd /
   mkdir -p /var/opt && wget -q -nc --no-check-certificate -P /var/opt https://github.com/openucx/ucx/releases/download/v$UCX_VERSION/ucx-$UCX_VERSION.tar.gz
   mkdir -p /var/opt && tar -x -f /var/opt/ucx-$UCX_VERSION.tar.gz -C /var/opt -z
   cd /var/opt/ucx-$UCX_VERSION &&   ./configure --prefix=/usr/local/ucx --disable-assertions --disable-debug --disable-doxygen-doc --disable-logging --disable-params-check --enable-optimizations  --without-cuda --enable-mt
   make -j$(nproc)
   make -j$(nproc) install
   rm -rf /var/opt/ucx-$UCX_VERSION /var/opt/ucx-$UCX_VERSION.tar.gz

   #
   # OpenMPI installation
   #
 
   export OMPI_DIR=/usr/local
   export OMPI_VERSION=5.0.3
   export OMPI_URL="https://download.open-mpi.org/release/open-mpi/v5.0/openmpi-$OMPI_VERSION.tar.bz2"
   echo "Installing OpenMPI version: " $OMPI_VERSION
   mkdir -p /opt/ompi
   cd /opt/ompi
   wget -c -O openmpi-$OMPI_VERSION.tar.bz2 $OMPI_URL && tar -xjf openmpi-$OMPI_VERSION.tar.bz2

   # Compile and install
   cd /opt/ompi/openmpi-$OMPI_VERSION
   ./configure --prefix=$OMPI_DIR --with-pmix=/usr/local --with-libevent=/usr --with-ompi-pmix-rte --with-orte=no --disable-oshmem --enable-mpirun-prefix-by-default --enable-shared  --without-verbs --with-hwloc --with-ucx=/usr/local/ucx --with-lustre --with-slurm --enable-mca-no-build=btl-uct 
   make -j$(nproc)
   make -j$(nproc) install

   # Set env variables so we can compile our applications
   export PATH=$OMPI_DIR/bin:$PATH
   export LD_LIBRARY_PATH=$OMPI_DIR/lib:$LD_LIBRARY_PATH
   export MANPATH=$OMPI_DIR/share/man:$MANPATH
   export COMPILER=gfortran


  # Install MPI based python modules
   export PATH=/usr/local/bin:$PATH
   export LD_LIBRARY_PATH=/usr/local/lib:/usr/local/lib64:$LD_LIBRARY_PATH
   python3 -m pip --no-cache-dir install mpi4py
   python3 -m pip --no-cache-dir install h5py   
   python3 -m pip --no-cache-dir install dask_mpi --upgrade
    

   #
   # Cmake
   #
  
   export CMAKE_DIR=/usr/local
   export CMAKE_VERSION=3.29.3
   echo "Installing CMake version: " $CMAKE_VERSION
   mkdir -p /opt/cmake
   cd /opt/cmake
   curl -OL https://github.com/Kitware/CMake/releases/download/v$CMAKE_VERSION/cmake-$CMAKE_VERSION-linux-x86_64.sh
   /bin/sh /opt/cmake/cmake-$CMAKE_VERSION-linux-x86_64.sh --prefix=/usr/local --skip-license 
   rm -rf /opt/cmake
   PATH=/usr/local/bin:$PATH

  #
  # HDF5
  #
  
  export HDF5_VERSION=1.14.3
  echo "Installing HDF5 version: " $HDF5_VERSION
  cd /opt
  wget -q -nc --no-check-certificate -L https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.14/hdf5-1.14.3/src/hdf5-$HDF5_VERSION.tar.bz2 
  tar -x -f hdf5-$HDF5_VERSION.tar.bz2
  cd hdf5-$HDF5_VERSION/
  ./configure --prefix=/usr/local/ --enable-parallel --enable-fortran --enable-fortran2003 --enable-shared CC=/usr/local/bin/mpicc CXX=/usr/local/bin/mpicxx FC=/usr/local/bin/mpif90 
  make -j$(nproc) 
  make -j$(nproc) install 
  rm -rf /opt/hdf5-$HDF5_VERSION/


  #
  # Darshan
  #
  export DARSHAN_VERSION=3.4.2
  echo "Installing Hypre version: " $HYPRE_VERSION
  cd /opt
  rm -rf /opt/darshan 
  git clone https://github.com/darshan-hpc/darshan.git
  cd /opt/darshan
  git checkout darshan-$DARSHAN_VERSION
  ./prepare.sh
  cd /opt/darshan/darshan-runtime
  ./configure --with-log-path-by-env=DARSHAN_LOG_PATH --with-jobid-env=SLURM_JOBID --enable-hdf5-mod --with-hdf5=/usr/local CC=mpicc
   make -j$(nproc)
   make -j$(nproc) install
  cd /opt/darshan/darshan-util
  ./configure
   make -j$(nproc)
   make -j$(nproc) install
   
  # py-darshan
   python3 -m pip --no-cache-dir install darshan	

  # 
  # Hypre
  #

  export HYPRE_VERSION=2.31.0
  echo "Installing Hypre version: " $HYPRE_VERSION
  cd /opt
  rm -rf /opt/hypre  
  git clone https://github.com/hypre-space/hypre.git
  cd /opt/hypre
  git checkout v2.31.0
  cd /opt/hypre/src/
  ./configure --prefix=/usr/local
  make install
  rm -rf /opt/hypre


  
  # Diagnostics I/O MPI-I/O

  #
  # IOR
  #
  
  rm -rf /opt/io
  mkdir -p /opt/io
  cd /opt/io
  git clone https://github.com/hpc/ior.git 
  cd ior/  
  ./bootstrap
  ./configure --with-lustre --with-hfd5 --prefix=/usr/local
  make -j$(nproc)
  make -j$(nproc) install
  rm -rf /opt/io


  #
  # ci_ompi
  #
  
  export CC=`which gcc`
  export CXX=`which g++`
  export PATH=$PATH:
  cd /opt
  tar xvf ci_ompi.tar
  mkdir build
  cd build
  cmake -DWITH_HDF5=ON -DWITH_FIO=ON -DCMAKE_INSTALL_PREFIX=/usr/local ../ci_ompi
  make -j$(nproc)
  make -j$(nproc) install
  rm -rf /opt/ci_ompi.tar

#
  # OSU microbenchmarks
  #

  export OSU_VERSION=7.4
  echo "Installing OSU Microbenchmarks version: " $OSU_VERSION
  cd /opt
  wget -c https://mvapich.cse.ohio-state.edu/download/mvapich/osu-micro-benchmarks-$OSU_VERSION.tar.gz
  tar xf osu-micro-benchmarks-$OSU_VERSION.tar.gz
  cd osu-micro-benchmarks-$OSU_VERSION/
  echo "Configuring and building OSU Micro-Benchmarks..."
  ./configure --prefix=/usr/local/osu CC=/usr/local/bin/mpicc CXX=/usr/local/bin/mpicxx CFLAGS=-I$(pwd)/util
  make -j$(nproc)
  make -j$(nproc) install
  # copy back all MPI benchmarks to /usr/local
  cp -R /usr/local/osu/libexec/osu-micro-benchmarks/mpi/* /usr/local/bin
  rm -rf /opt/osu-micro-benchmarks-$OSU_VERSION/  



%runscript
exec /usr/local/bin/$*


